﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace caaps.Api.Controllers
{
    public class Response
    {
        public class CaapsResponse : EmptyResponse
        {
            public object Data { get; set; }
        }
    }
}
