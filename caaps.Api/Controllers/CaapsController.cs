﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using caaps.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static caaps.Api.Controllers.Response;

namespace caaps.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaapsController : Controller
    {
        private readonly DB.CaapsRepository caapsRepository;

        public CaapsController()
        {
            this.caapsRepository = new DB.CaapsRepository();
        }

        [HttpGet("simple")]
        public object GetTipodeArchivo()
        {



            List<tipoArchivo> tipoarchivo = new List<tipoArchivo>();


            var CaapsRepository = new DB.CaapsRepository();
            var Data = CaapsRepository.tipoArchivo();
            foreach (var obj in Data)
            {
                tipoarchivo.Add(obj);
            }

            return new CaapsResponse { Status = ResponseStatuses.Correcto.ToString(), Data = tipoarchivo };


        }
        [HttpGet("tipoarchivodb")]
        public object GetTipodeArchivodb()
        {



            List<tipoArchivo> tipoarchivodb = new List<tipoArchivo>();


            var CaapsRepository = new DB.CaapsRepository();
            var Data = CaapsRepository.tipoArchivodb();
            foreach (var obj in Data)
            {
                tipoarchivodb.Add(obj);
            }

            return new CaapsResponse { Status = ResponseStatuses.Correcto.ToString(), Data = tipoarchivodb };


        }


    }
}