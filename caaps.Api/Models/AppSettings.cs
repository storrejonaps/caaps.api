﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace caaps.Api.Models
{
    public class AppSettings
    {
        public static string ConnectionString_Api { get; set; }
        public static string ConnectionString_Caaps { get; set; }
    }
}
