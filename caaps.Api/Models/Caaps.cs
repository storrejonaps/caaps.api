﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace caaps.Api.Models
{
    public class tipoArchivo
    {
        public string id { set; get; }
        public string nombreTipoarchivo { set; get; }
    }
    public class entidad
    {
        public string id { set; get; }
        public string nombre_entidad { set; get; }
    }
}
