﻿using caaps.Api.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace caaps.Api.DB
{
    public class CaapsRepository
    {
        internal IDbConnection Connection
        {
            get { return new SqlConnection(AppSettings.ConnectionString_Caaps); }
        }

        public IEnumerable<tipoArchivo> tipoArchivo()
        {
            List<tipoArchivo> tipoArchivo = new List<tipoArchivo>();

            tipoArchivo.Add(new tipoArchivo { id = "4", nombreTipoarchivo = "FUT" });


            return tipoArchivo;
        }

        public IEnumerable<tipoArchivo> tipoArchivodb()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var data = dbConnection.Query<tipoArchivo>(
                     @"SELECT  id,nombreTipoarchivo
                    FROM tipoarchivo");
                return data;
            }
        }
    }
}
